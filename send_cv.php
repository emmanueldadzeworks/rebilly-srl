<?php

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://applybyapi.com/apply/",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_POST => true,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_HTTPHEADER => array(
        "Content-Type:multipart/form-data"
    ),
    CURLOPT_POSTFIELDS =>  array(
        // 'resume' => new \CURLFILE('/C:/Users/emmanuel.dadzie/Downloads/cv.pdf'),
        'resume' => file_get_contents('cv.pdf'),
        'token' => 'J9ZOXF2XBCYCMW9GONUJ',
        'name' => 'Emmanuel Kwabena Dadzie',
        'email' => 'emmanueldadzeworks@gmail.com',
        'phone' => '00233209137654',
    ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
// var_dump($response);
